$(document).ready(function () {

    /* Owl Carousel
     ************************/
    var $carousel = $('.project-carousel');
    $carousel.each(function () {
        var autoplay = true;
        var $this = $(this);

        if ($('.fitvids', $this).length > 0)
            autoplay = false;

        $this.owlCarousel({
            items: 1,
            singleItem: true,
            slideSpeed: 800,
            autoPlay: autoplay,
            pagination: true
        });
    });

});