$(document).ready(function () {

    /* Owl Carousel
     ************************/
    if ($('.owl-carousel').length > 0) {
        $(".owl-carousel").owlCarousel({
            items: 4,
            autoPlay: true,
            pagination: false
        });
    }

});