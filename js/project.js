jQuery(function ($) {

    /***********************************
     Global Variables
     ************************************/
    var $window = $(window);
    var $body = $('body');

    /***********************************
     Scrollers
     ************************************/
    function scrollTo(object, speed) {
        var $object;
        var scroll;
        if (typeof speed === "undefined" || speed === null) {
            speed = 1500;
        }

        if (typeof object === 'string') {
            $object = $(object);
            scroll = $object.offset().top - 70;
        } else if (object instanceof $) {
            $object = object;
            scroll = $object.offset().top - 70;
        } else if ($.isNumeric(object)) {
            scroll = object;
        } else {
            $object = $('body');
            scroll = $object.offset().top - 70;
        }

        scroll = (scroll >= 0) ? scroll : 0;

        $('body, html').animate({
            scrollTop: scroll
        }, speed);
    }

    $('a[data-scrollTo]').click(function (e) {
        var target = $(this).attr('data-scrollTo');
        scrollTo(target);
        e.preventDefault();
    });

    $('#backtotop').click(function () {
        scrollTo(0);
    });

    $('.next-section').click(function () {
        var $btn = $(this);
        var $parent = $btn.parents('section');
        var parentindex = $('section').index($parent);
        var $nextparent = $('section').eq(parentindex + 1);
        scrollTo($nextparent);
    });

    /***********************************
     Fitvids
     ************************************/
    if ($('.fitvids').length > 0) {
        $('.fitvids').fitVids();
    }


    /***********************************
     Element Animation
     ************************************/
    if (jQuery.browser.mobile) {
        $('[data-animate]').css('visibility', 'visible')
    }

    function animate() {
        $('[data-animate]').each(function () {
            if ($(this).isOnScreen()) {
                var $this = $(this);
                var animation = $this.attr('data-animate');
                var delay = $this.attr('data-animate-delay') ? $this.attr('data-animate-delay') : 0;
                setTimeout(function () {
                    $this.addClass('animated').addClass(animation);
                }, delay);
            }
        });
    }

    /***********************************
     Content Block Parallax
     ************************************/
    $('.parallax').each(function () {
        var $this = $(this);
        var parallax = $this.attr('data-parallax') ? $this.attr('data-parallax') : 0.3;
        $this.parallax("50%", parallax, true);
    });

    /***********************************
     Portfolio Lightbox
     ************************************/
    $('.portfolio').each(function () {
        $(this).magnificPopup({
            delegate: '.portfolio-project-lightbox',
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });

    $('.btn-lightbox').each(function () {
        $(this).magnificPopup({
            type: 'image',
            gallery: {
                enabled: false
            }
        });
    });


    /***********************************
     Video Background
     ************************************/
    $('.video-bg').each(function () {
        var mp4 = $(this).attr('data-video-mp4');
        var ogv = $(this).attr('data-video-ogv');
        var webm = $(this).attr('data-video-webm');
        var jpg = $(this).attr('data-video-jpg');
        $(this).videoBG({
            mp4: mp4,
            ogv: ogv,
            webm: webm,
            poster: jpg,
            scale: true,
            zIndex: 0
        });
    });

    /***********************************
     Window Binding
     ************************************/
    $window.scroll(function (e) {
        var scrolled = $window.scrollTop();
        if (!jQuery.browser.mobile) {
            animate();
        }
        if (scrolled > 0) {
            $('#navbar').removeClass('navbar-lg');
        } else {
            $('#navbar').addClass('navbar-lg');
        }

        if (scrolled > 100) {
            $('#backtotop').removeClass('opacity-hide');
        } else {
            $('#backtotop').addClass('opacity-hide');
        }
    }).trigger('scroll');
    $window.resize(function () {

    }).trigger('resize');

});